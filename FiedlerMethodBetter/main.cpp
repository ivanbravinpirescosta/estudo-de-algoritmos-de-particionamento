#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>

#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>

using namespace std;
int** matrizAdjacencia;

int calcula_corte(double* autoVetor, int num_nos){
    int vext=0;
    int cont =0, c1=0, c2=0;
    int * n1aux = new int [num_nos/2];
    int * n2aux = new int [num_nos/2];
    int found_v=-1,found_i=-1;
    //Inicia a particao
    while(cont<num_nos){
        if ((autoVetor[cont]>0)&&(c1<num_nos/2)){
            n1aux[c1]=cont;
            c1++;
        }else{
            if(c2<num_nos/2){
                n2aux[c2]=cont;
                c2++;
            }
            else{
                n1aux[c1]=cont;
                c1++;
            }
        }
        cont++;
    }

    for(int v=0; v<num_nos; v++){
        for(int i=0; i<num_nos; i++){
            for(int j=0; (j<(num_nos/2)) && (found_i == -1 || found_v == -1); j++){
                if(v==n1aux[j] || v==n2aux[j] )
                    found_v=j;
                if(i==n1aux[j] || i==n2aux[j] )
                    found_i=j;
            }//end for j
            if(((v==n1aux[found_v]) && (i==n2aux[found_i]))||((v==n2aux[found_v]) && (i==n1aux[found_i]))){
                vext = vext + matrizAdjacencia[v][i];
            }//end if
            found_i=found_v=-1;
        }//end for i
    }//end for v

    delete n1aux;
    delete n2aux;

    return vext/2;

}// end calcula corte

int entradaMatrizAdjacencia(char* entrada, int *ordem){
    ifstream ifs;
    ifs.open (entrada, ifstream::in);
    if(ifs == NULL)
     return -1;

    ifs >> *ordem;
    if(*ordem<0)
        return -1;

    //Aloca Grafo
    matrizAdjacencia = new int* [(*ordem)];
    for (int i=0; i<(*ordem); i++)
        matrizAdjacencia[i] = new int [(*ordem)];

    for(int i=0; i<*ordem; i++){
        for(int j=0; j<i; j++){
            ifs >>  matrizAdjacencia[i][j];
            matrizAdjacencia[j][i] = matrizAdjacencia[i][j];
        }//end for j
        ifs >>  matrizAdjacencia[i][i];
    }//end for i
    return 0;
}

void Calcula_AutoValor_e_AutoVetor_libGSL (double *matriz_Adjacencia_linear, int ordem, double& AutoValor, double *AutoVetor) {
    gsl_matrix_view m = gsl_matrix_view_array (matriz_Adjacencia_linear, ordem, ordem);
    gsl_vector *eval = gsl_vector_alloc (ordem);
    gsl_matrix *evec = gsl_matrix_alloc (ordem, ordem);
    gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (ordem);

    gsl_eigen_symmv (&m.matrix, eval, evec, w);
    gsl_eigen_symmv_free (w);
    gsl_eigen_symmv_sort (eval, evec, GSL_EIGEN_SORT_ABS_ASC);

    //calcula os auto valores
    AutoValor = gsl_vector_get(eval, 1);

    //Calcula o auto vetor
    for (int j=0; j<ordem; j++){
        AutoVetor[j] = gsl_matrix_get(evec, j, 1);
    }

    //libera memória temporária
    gsl_vector_free (eval);
    gsl_matrix_free (evec);
}

int main(int argc, char* argv[]){
    timespec timeIni,timeEnd;
    int ordem;
    double *matriz_Adjacencia_Linear;
    double autoValor, *autoVetor;

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeIni);
    //Inicio - leitura da matriz de adjacencia e alocacao na memória
    if (entradaMatrizAdjacencia(argv[1],&ordem)==-1)
        return -2;

    matriz_Adjacencia_Linear = new double[ordem* ordem];
    for (int i=0; i<ordem; i++)
        for (int j=0; j<ordem; j++)
            matriz_Adjacencia_Linear[i*ordem + j]=matrizAdjacencia[i][j];

    autoVetor = new double[ordem];
    Calcula_AutoValor_e_AutoVetor_libGSL (matriz_Adjacencia_Linear, ordem, autoValor, autoVetor);

    //int corte = calcula_corte(AutoVetor,Ordem);

//    cout<<"corte de linha FIM: "<< calcula_corte(AutoVetor,ordem)<<endl;
    cout<< calcula_corte(autoVetor,ordem)<<endl;
    //limpa memória
    for(int i=0; i<ordem; i++)
    delete matrizAdjacencia[i];
        delete matrizAdjacencia;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
    cout << ( (timeEnd.tv_sec - timeIni.tv_sec) ) + ( (timeEnd.tv_nsec - timeIni.tv_nsec)/ 1e9) << endl;

//    cout<<"tempo de execucao (seg): "<<  ( (timeEnd.tv_sec - timeIni.tv_sec) ) + ( (timeEnd.tv_nsec - timeIni.tv_nsec)/ 1e9) <<" seg"<<endl;
    return 0;

}
