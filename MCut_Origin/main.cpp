#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>

#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>


using namespace std;
int** matrizAdjacencia;

struct autovetorIndice {
  double autovetor;
  int indice;
};

double calcula_corte(int *n1aux, int *n2aux, int num_nos){
    int vext=0;
    int found_v=-1,found_i=-1;

    for(int v=0; v<num_nos; v++){
        for(int i=0; i<num_nos; i++){
            for(int j=0; (j<(num_nos/2)) && (found_i == -1 || found_v == -1); j++){
                if(v==n1aux[j] || v==n2aux[j] )
                    found_v=j;
                if(i==n1aux[j] || i==n2aux[j] )
                    found_i=j;
            }//end for j
            if(((v==n1aux[found_v]) && (i==n2aux[found_i]))||((v==n2aux[found_v]) && (i==n1aux[found_i]))){
                vext = vext + matrizAdjacencia[v][i];
            }//end if
            found_i=found_v=-1;
        }//end for i
    }//end for v
    return vext/2;
}// end calcula corte

int entradaMatrizAdjacencia(char* entrada, int *ordem){
    ifstream ifs;
    ifs.open (entrada, ifstream::in);
    if(ifs == NULL)
     return -1;

    ifs >> *ordem;
    if(*ordem<0)
        return -1;

    //Aloca Grafo
    matrizAdjacencia = new int* [(*ordem)];
    for (int i=0; i<(*ordem); i++)
        matrizAdjacencia[i] = new int [(*ordem)];

    for(int i=0; i<*ordem; i++){
        for(int j=0; j<i; j++){
            ifs >>  matrizAdjacencia[i][j];
            matrizAdjacencia[j][i] = matrizAdjacencia[i][j];
        }//end for j
        ifs >>  matrizAdjacencia[i][i];
    }//end for i
    return 0;
}

void Calcula_AutoValor_e_AutoVetorGSL (double *matrizAdjacenciaLinear, int Ordem, double& autoValor, double *autoVetor) {

   gsl_matrix_view m = gsl_matrix_view_array (matrizAdjacenciaLinear, Ordem, Ordem);
   gsl_vector *eval = gsl_vector_alloc (Ordem);
   gsl_matrix *evec = gsl_matrix_alloc (Ordem, Ordem);
   gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (Ordem);

   gsl_eigen_symmv (&m.matrix, eval, evec, w);
   gsl_eigen_symmv_free (w);
   gsl_eigen_symmv_sort (eval, evec, GSL_EIGEN_SORT_ABS_ASC);

    autoValor = gsl_vector_get(eval, 1);

    for (int j=0; j<Ordem; j++){
        autoVetor[j] = gsl_matrix_get(evec, j, 1);
    }

    gsl_vector_free (eval);
    gsl_matrix_free (evec);

}

int particao(autovetorIndice *vetor, int esq, int dir){
    int i=esq, j=dir;
    autovetorIndice pivo;
    autovetorIndice temp;

    pivo.autovetor = vetor[esq].autovetor;
    pivo.indice = vetor[esq].indice;

    while(i<j){
        while((vetor[i].autovetor<=pivo.autovetor)&&(i<dir))
            i++;
        while(vetor[j].autovetor>pivo.autovetor)
            j--;

        if(i<j){
            temp.autovetor = vetor[i].autovetor;
            temp.indice = vetor[i].indice;
            vetor[i].autovetor = vetor[j].autovetor;
            vetor[i].indice = vetor[j].indice;
            vetor[j].autovetor = temp.autovetor;
            vetor[j].indice = temp.indice;
        }//end if
    }//end while

    vetor[esq].autovetor = vetor[j].autovetor;
    vetor[esq].indice = vetor[j].indice;
    vetor[j].autovetor = pivo.autovetor;
    vetor[j].indice = pivo.indice;
    return j;
}//end particão

void qSort(autovetorIndice *vetor, int esq, int dir){
    int meio;
    if(esq<dir){
        meio = particao(vetor, esq, dir);
        qSort(vetor, esq, meio-1);
        qSort(vetor, meio+1, dir);
    }//end if
}//end qSort

void quickSort(autovetorIndice *vetor, int n){
     qSort(vetor,0,n-1);
}

void ordenaEParticiona(double *autovetor, int *n1, int *n2, int ordem){
    autovetorIndice *vet;
    vet = new autovetorIndice[ordem];

    for(int i=0; i<ordem; i++){
        vet[i].indice = i;
        vet[i].autovetor = autovetor[i];
    }

    quickSort(vet, ordem);
    for(int i=0; i<ordem/2; i++){
        n1[i] = vet[i].indice;
        n2[i] = vet[(ordem/2)+i].indice;
    }//end for
}//end ordena


int main(int argc, char* argv[]){
    timespec timeIni,timeEnd;
    int ordem;
    double *matriz_Adjacencia_Linear;
    int *n1;
    int *n2;
    double AutoValor, *AutoVetor;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeIni);
    //Inicio - leitura da matriz de adjacencia e alocacao na memória
    if (entradaMatrizAdjacencia(argv[1],&ordem)==-1)
        return -2;

    matriz_Adjacencia_Linear = new double[ordem* ordem];
    for (int i=0; i<ordem; i++)
        for (int j=0; j<ordem; j++)
            matriz_Adjacencia_Linear[i*ordem + j]=matrizAdjacencia[i][j];


    n1 = new int[ordem/2];
    n2 = new int[ordem/2];

    AutoVetor = new double[ordem];

    Calcula_AutoValor_e_AutoVetorGSL(matriz_Adjacencia_Linear, ordem,AutoValor, AutoVetor);
    ordenaEParticiona(AutoVetor, n1, n2, ordem);


//    cout<<"\ncorte de linha FIM: "<< calcula_corte(n1, n2, ordem)<<endl;
    cout<<calcula_corte(n1, n2, ordem)<<endl;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
    cout<<( (timeEnd.tv_sec - timeIni.tv_sec) ) + ( (timeEnd.tv_nsec - timeIni.tv_nsec)/ 1e9)<<endl;

//    cout<<"tempo de execucao (seg): "<<  ( (timeEnd.tv_sec - timeIni.tv_sec) ) + ( (timeEnd.tv_nsec - timeIni.tv_nsec)/ 1e9) <<" seg"<<endl;
    return 0;
}
