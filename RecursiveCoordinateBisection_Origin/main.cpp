// Recursive Coordinate Bisection
// Rafael Renó Faria
#include<iostream>
#include <time.h>
#include <fstream>
using namespace std;

// definindo uma estruturausado como uma lista ao fim do algoritmo


// variáveis globais
int num_nos;             //numero de nos do grafo
int **matriz_adj;             //guarda o grafo na memoria
int *n1;                 //bipartiçăo 1
int *n2;                 //bipartiçăo 2
int *eixox;
int *eixoy;
int *eixoz;
int *eixoCoordenado;

//funçőes
void aloca_grafo();  //essa funçăo aloca também a variavel lista
int entradaMatrizAdjacencia(char* entrada, int *num_nos);
void entradaMatrizAdjacenciaPosicaoGeografica();
void ler_matriz_de_adjacencias();
void imprime_grafo(int nnos);    //essa funçăo pede o numero de nós pois assim é possivel imprimir um subgrafo
void desaloca();
int calcula_corte();
void bubbleSort(int *vetor);
void biparticao(int *eixomod, int *eixooriginal);

//funçăo main
int main(int argc, char* argv[]){
    int timeini;
    int minx,miny,minz,maxx,maxy,maxz;
    int *eixoAux;
    float pontoMedio;

    timeini=clock();

    if (entradaMatrizAdjacencia(argv[1],&num_nos )==-1)
        return -2;
    entradaMatrizAdjacenciaPosicaoGeografica();

    eixoCoordenado = new int[num_nos];
    eixoAux = new int[num_nos];

    // começo do algoritmo
    minx=eixox[0];
    maxx=eixox[0];
    miny=eixoy[0];
    maxy=eixoy[0];
    minz=eixoz[0];
    maxz=eixoz[0];

    for(int i=1; i<num_nos; i++){

                    if(eixox[i]<minx)
                    minx=eixox[i];

                    if(eixox[i]>maxx)
                    maxx=eixox[i];

                    if(eixoy[i]<miny)
                    miny=eixoy[i];

                    if(eixoy[i]>maxy)
                    maxy=eixoy[i];

                    if(eixoz[i]<minz)
                    minz=eixoz[i];

                    if(eixoz[i]>maxz)
                    maxz=eixoz[i];

                    }//feça for i

                    if((maxx-minx)>(maxy-miny)){
                                                if((maxx-minx)>(maxz-minz)){
                                                //eixo x maior disperçăo

                                                for(int i=0; i<num_nos; i++)
                                                        eixoCoordenado[i] = eixox[i];

                                                }else{
                                                      //eixo z maoir disperçăo

                                                for(int i=0; i<num_nos; i++)
                                                        eixoCoordenado[i] = eixoz[i];
                                                      }
                    }else{
                                                if((maxy-miny)>(maxz-minz)){
                                                //eixo y maior disperçăo

                                                for(int i=0; i<num_nos; i++)
                                                        eixoCoordenado[i] = eixoy[i];
                                                }else{
                                                      //eixo z maior disperçăo

                                                for(int i=0; i<num_nos; i++)
                                                        eixoCoordenado[i] = eixoz[i];
                                                      }
                    }

    for(int i=0; i<num_nos; i++)
       eixoAux[i] = eixoCoordenado[i];

    bubbleSort(eixoCoordenado);

    biparticao(eixoCoordenado, eixoAux);

    cout<<"\n\ncorte de linha "<<calcula_corte()<<"\n";
    desaloca();
    delete eixoCoordenado;
    cout<<"tempo de execucao "<<(clock()-timeini)*1000/CLOCKS_PER_SEC;

    return 0;
}// fim da funçăo main

void aloca_grafo(){
matriz_adj = new int* [num_nos];

     for(int i=0; i<num_nos; i++){
                     matriz_adj[i] = new int [num_nos];
                     }//end for i

n1 = new int[num_nos/2];
n2 = new int[num_nos/2];
eixox = new int[num_nos];
eixoy = new int[num_nos];
eixoz = new int[num_nos];

     }//fim aloca

void ler_matriz_de_adjacencias(){
     for(int i=0; i<num_nos; i++){
             for(int j=0; j<=i; j++){
                     cin>>matriz_adj[i][j];
                     matriz_adj[j][i] = matriz_adj[i][j];
                     }//end for j
             }//end for i
     for(int i=0; i<num_nos; i++)
             cin>>eixox[i];
     for(int i=0; i<num_nos; i++)
             cin>>eixoy[i];
     for(int i=0; i<num_nos; i++)
             cin>>eixoz[i];

     }//fim matriz

void imprime_grafo(int nnos){
     for(int i=0; i<nnos; i++){
             for(int j=0; j<nnos; j++)
                     cout<<matriz_adj[i][j];
                     cout<<"\n";
             }//end for i
     }//fim imprime

void desaloca(){
     delete n1;
     delete n2;
     delete eixox;
     delete eixoy;
     delete eixoz;

     for(int i=0; i<num_nos; i++)
          delete matriz_adj[i];
     delete matriz_adj;
          }//fim desaloca

int calcula_corte(){
    int vext=0;

    for(int v=0; v<num_nos; v++){
    for(int i=0; i<num_nos; i++){
    for(int j=0; j<(num_nos/2); j++){
    for(int k=0; k<(num_nos/2); k++){

            if(((v==n1[j]) && (i==n2[k]))||((v==n2[j]) && (i==n1[k]))){
                                                       vext = vext + matriz_adj[v][i];
            }//end if

    }//end for k
    }//end for j
    }//end for i
    }//end for v
    return vext/2;

}// end calcula corte

void bubbleSort(int *vetor){
 int aux, i, j;
 int n = num_nos;

 for(i=0; i<n; i++){
 for(j=0; j<(n-1-i); j++){
 if (vetor[j+1] < vetor[j]){
 aux=vetor[j];
 vetor[j]=vetor[j+1];
 vetor[j+1]=aux;
 }//fim if
 }//fim for j
 }//fim for i
 }//fim bubbleSort

 void biparticao(int *eixomod, int *eixooriginal){
       int aux = 0;
       int cont1=0;
       int cont2=0;



       aux = (eixomod[num_nos/2] + eixomod[(num_nos/2) - 1])/2;


       for(int i=0; i<num_nos; i++){

               if(eixooriginal[i]>aux){

                               n1[cont1]=i;

                               cont1++;
                               }else{
                               n2[cont2]=i;
                               cont2++;
                               }
               }//end for


       }//fim biparticao
 int entradaMatrizAdjacencia(char* entrada, int *num_nos){
       ifstream ifs;
       ifs.open (entrada, ifstream::in);
       if(ifs == NULL)
          return -1;

       ifs >> *num_nos;
       if(num_nos<0)
               return -1;

       aloca_grafo();
       for(int i=0; i<*num_nos; i++){
          for(int j=0; j<i; j++){
             ifs >>  matriz_adj[i][j];
             matriz_adj[j][i] = matriz_adj[i][j];
           }//end for j
           ifs >>  matriz_adj[i][i];
       }//end for i
       return 0;
 }//fim_entradaMatrizAdjacencia
 void entradaMatrizAdjacenciaPosicaoGeografica(){

     for(int i=0; i<num_nos; i++)
             cin>>eixox[i];
     for(int i=0; i<num_nos; i++)
             cin>>eixoy[i];
     for(int i=0; i<num_nos; i++)
             cin>>eixoz[i];
}
     /*obs.: 1- O algoritmo foi construido levando em consideraçăo que existe um numero par de nós no grafo
             */
