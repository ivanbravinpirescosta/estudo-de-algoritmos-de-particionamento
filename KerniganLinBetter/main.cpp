/*
 *
 * Algoritmo de particionamento: Kernighan Lin 2
 * Criadores: Ivan Bravin Pires Costa e Rafael Renó Faria
 * Data: 11/09/2013
 *
 */

#include <iostream>
#include <fstream>
#include <time.h>
using namespace std;

// definindo uma estruturausado como uma lista ao fim do algoritmo
typedef struct{
        int v1;
        int v2;
        int ganho;
}par_ganho;

// variáveis globais
int num_nos;                        //numero de nos do grafo
int **matriz_adj;                   //guarda o grafo na memoria, em forma de matriz de adjacencia
int *n1;                            //bipartição 1
int *n2;                            //bipartição 2
int *n1aux;                         //bipartição auxiliar 1
int *n2aux;                         //bipartição auxiliar 2
par_ganho *lista;                   //vetor com os ganhos maximos de cada interação e os respectivos nos
int somaMaxima=0;                   //soma maxima dos ganhos no vetor de somas maximas

//funções
void aloca_grafo();                 //essa função aloca também a variavel lista
void ler_matriz_de_adjacencias();
void imprime_grafo(int nnos);       //essa função pede o numero de nós pois assim é possivel imprimir um subgrafo
void inicializa_biparticao();
int ganho_de_um_no(int v);
int ganho_da_permuta(int v1, int v2, int g1, int g2);
int posicao_da_soma_maxima();
void limparMemoria();
int calcula_corte();
void bubbleSort(int *vetor);
int entradaMatrizAdjacencia(char* entrada,int *num_nos);

//função main
int main(int argc, char* argv[]){
    par_ganho aux;   //auxilia na seleção do ganho maximo com os nos
    int ganhoaux;    //auxilia na determinação do ganho maximo
    int posSomaMaxima = -1;  //marca a posição do vetor de ganhos cuja soma dos ganhos é maxima
    int g1;          // marca o ganho unitario de um no
    timespec timeIni,timeEnd;

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeIni);

    //Inicio - leitura da matriz de adjacencia e alocacao na memória
    if (entradaMatrizAdjacencia(argv[1],&num_nos )==-1)
        return -2;
    inicializa_biparticao();

//    cout<<"\ncorte de linha Inicial:"<<calcula_corte()<<endl;

    // começo do algoritmo
    do{
        for(int i=0; i<(num_nos/2); i++){
            int idx_n1aux = -1;
            int idx_n2aux = -1;
            aux.ganho = -2*num_nos;
            aux.v1 = -1;
            aux.v2 = -1;
            for(int j=0; j<(num_nos/2); j++){
                g1 = ganho_de_um_no(n1aux[j]);
                   for(int k=0; k<(num_nos/2); k++){
                            int control = 0;
                            for(int l=0; (l<i) && (control<0); l++){
                                if( (n1aux[j]== lista[l].v2) || (n2aux[k]== lista[l].v1) ){
                                    control++;
                                }
                            }//end for l
                            if(control==0)
                                ganhoaux = ganho_da_permuta(n1aux[j], n2aux[k], g1, ganho_de_um_no(n2aux[k]));

                            if( (ganhoaux > aux.ganho) ){         //if 2
                                aux.ganho = ganhoaux;
                                aux.v1 = n1aux[j];
                                aux.v2 = n2aux[k];
                                idx_n1aux = j;
                                idx_n2aux = k;
                            }//end if 2

                   }//end for k
               }//end for j

               n1aux[idx_n1aux] = aux.v2;
               n2aux[idx_n2aux] = aux.v1;

               lista[i].ganho = aux.ganho;    //inserção na lista é invertida em relação à partição inicial
               lista[i].v1 = aux.v1;
               lista[i].v2 = aux.v2;

        }//end for i

        posSomaMaxima = posicao_da_soma_maxima();

        // efetua as permutacões
        if(posSomaMaxima>=0){
           for(int k=0; k<=posSomaMaxima; k++){
                   for(int j=0; j<(num_nos/2); j++){

                        if(n1[j] == lista[k].v1)
                            n1[j] = lista[k].v2;

                        if(n2[j] == lista[k].v2)
                            n2[j] = lista[k].v1;

                   n1aux[j] = n1[j];
                   n2aux[j] = n2[j];
                   }//end for j
            }// end for k

        }   //end if

    }while(somaMaxima>0);
    bubbleSort(n1);
    bubbleSort(n2);

//    cout<<"corte de linha FIM:"<<calcula_corte()<<endl;
    cout<<calcula_corte()<<endl;
    limparMemoria();
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
    cout<<  ( (timeEnd.tv_sec - timeIni.tv_sec) ) + ( (timeEnd.tv_nsec - timeIni.tv_nsec)/ 1e9)<<endl;

//    cout<<"tempo de execucao (seg): "<<  ( (timeEnd.tv_sec - timeIni.tv_sec) ) + ( (timeEnd.tv_nsec - timeIni.tv_nsec)/ 1e9) <<" seg"<<endl;
   return 0;
}// fim da função main


int entradaMatrizAdjacencia(char* entrada, int *num_nos){
      ifstream ifs;
      ifs.open (entrada, ifstream::in);
      if(ifs == NULL)
         return -1;

      ifs >> *num_nos;
      if(num_nos<0)
              return -1;

      aloca_grafo();
      for(int i=0; i<*num_nos; i++){
         for(int j=0; j<i; j++){
            ifs >> matriz_adj[i][j];
            matriz_adj[j][i] = matriz_adj[i][j];
          }//end for j
          ifs >> matriz_adj[i][i];
      }//end for i
      return 0;
}

/*    TO-DO - paralelizar esta funcao; IPP or TBB ?
 *
 */
void aloca_grafo(){
    matriz_adj = new int* [num_nos];
    for(int i=0; i<num_nos; i++){
        matriz_adj[i] = new int [num_nos];
    }//end for i
    lista = new par_ganho [num_nos/2];
}//fim aloca

/*   TO-DO: Melhorar performace;
 *   Implementado(OK): Retirar a alocacao ambigua quando i==j.
 */
void ler_matriz_de_adjacencias(){
    for(int i=0; i<num_nos; i++){
        for(int j=0; j<i; j++){
          //  cout<<"Inserir ("<<i<<")("<<j<<"):";
            cin>>matriz_adj[i][j];
            matriz_adj[j][i] = matriz_adj[i][j];
        }//end for j
      //  cout << "Inserir (" << i << ")(" << i << "):";
        cin >> matriz_adj[i][i];
    }//end for i
}//fim matriz

void imprime_grafo(int nnos){
     for(int i=0; i<nnos; i++){
             for(int j=0; j<nnos; j++)
                     cout<<matriz_adj[i][j];
             cout<<"\n";
     }//end for i
}//fim imprime_grafo

/*
 *   TO-DO - paralelizar esta funcao; IPP or TBB ?
 */
void inicializa_biparticao(){
     n1 = new int [num_nos/2];
     n2 = new int [num_nos/2];

     n1aux = new int [num_nos/2];
     n2aux = new int [num_nos/2];

     for(int i=0; i<num_nos/2; i++){
             n1aux[i]=n1[i]=2*i;
             n2aux[i]=n2[i]=(2*i)+1;
     }//end for i
}//fim inicializa_biparticao

/*   TO-DO: Melhorar performace;
 *   Implementado(OK): Diminiur ordem da funcao
 */
int ganho_de_um_no(int v){
     int vint=0;
     int vext=0;

     // marcar em qual lado da biparticao esta: n1aux(DIR)=0, n2aux(ESQ)=1;
     int ladoEsqDir=-1;

     //esta interacao tem o propósito de encontrar o nó v(entrada), nas matrizes biparidas n1aux e n2aux.
     /*  TO-DO: paralelizar, com sincronizacao
      *
      */
     for (int j = 0; (j < num_nos/2) && (ladoEsqDir < 0);++j) {
         if (v == n1aux[j]) {
             ladoEsqDir=0;
         }
         if (v == n2aux[j]) {
             ladoEsqDir=1;
          }
     }//fim laco j;

    //caso não encontre o nó v nas biparticoes
    if(ladoEsqDir==-1) return -1;

    //Nesse laco encontra-se em qual lado o
    for(int i=0; i<num_nos; i++){
        for(int j=0; (j<(num_nos/2)); j++){
            if( (!ladoEsqDir && i==n1aux[j]) || (ladoEsqDir && i==n2aux[j])){
                 vint = vint + matriz_adj[v][i];
                 break;
            }
            if( (ladoEsqDir && i==n1aux[j]) || (!ladoEsqDir && i==n2aux[j])){
                 vext = vext + matriz_adj[v][i];
                 break;
            }
        }//end for j
    }//end for i
    return vext-vint;
}//fim ganho_de_um_no

int ganho_da_permuta(int v1, int v2, int g1, int g2){
    if(matriz_adj[v1][v2]!=0)
        return g1 + g2 - (2*matriz_adj[v1][v2]);
    else
        return g1 + g2;
}//fim - ganho_da_permuta


/*  TO-DO: paralizacao IPP ou TBB?
 *       -> método de maximo em vetor
 */
int posicao_da_soma_maxima(){
    int somaMaximaAtual=0;
    int pos=0;
    somaMaxima=0;
    for(int i=0; i<(num_nos/2); i++){
            somaMaximaAtual=somaMaximaAtual+lista[i].ganho;
            if(somaMaximaAtual>=somaMaxima){
                 somaMaxima = somaMaximaAtual;
                 pos=i;
            }
    }//end for i
    return pos;
}//fim da posição da soma maxima

void limparMemoria(){
     delete n1;
     delete n2;
     delete n1aux;
     delete n2aux;
     delete lista;

     for(int i=0; i<num_nos; i++)
          delete matriz_adj[i];
     delete matriz_adj;
}//fim desaloca

/*  TO-DO: melhorar perfomace e paralelizar IPP ou TBB
 *
 */
int calcula_corte(){
    int vext=0;
    int found_v=-1,found_i=-1;

    for(int v=0; v<num_nos; v++){
        for(int i=0; i<num_nos; i++){
            for(int j=0; (j<(num_nos/2)) && (found_i == -1 || found_v == -1); j++){
                if(v==n1aux[j] || v==n2aux[j] )
                    found_v=j;
                if(i==n1aux[j] || i==n2aux[j] )
                    found_i=j;
            }//end for j
            if(((v==n1aux[found_v]) && (i==n2aux[found_i]))||((v==n2aux[found_v]) && (i==n1aux[found_i]))){
                vext = vext + matriz_adj[v][i];
            }//end if
            found_i=found_v=-1;
        }//end for i
    }//end for v
    return vext/2;
}// end calcula_corte

void bubbleSort(int *vetor){
    int aux, i, j;
    int n = num_nos/2;

    for(i=0; i<n; i++){
        for(j=0; j<(n-1-i); j++){
            if (vetor[j+1] < vetor[j]){
                aux=vetor[j];
                vetor[j]=vetor[j+1];
                vetor[j+1]=aux;
            }//fim if
        }//fim for j
    }//fim for i
}//fim bubbleSort

/*   Cbs.: 1- O algoritmo foi construido levando em consideração que existe um numero par de nós no grafo
 */
